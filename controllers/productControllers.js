const Product = require('../models/Product');
const bcrypt = require('bcryptjs');
const auth = require('../auth');
// const { reset } = require('nodemon');

// CREATE PRODUCT ADMIN ONLY
module.exports.addProduct = (req, res) => {
    console.log(req.body)
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category
     })

            newProduct.save()
                .then(result => res.send(result))
                .catch(err => res.send(err))
        };
    
// GET ALL PRODUCTS
module.exports.getAllProducts = (req, res) => {
    Product.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
};



module.exports.search = (req, res) => {
    console.log(req.body)
    Product.find({name : {$regex : req.body.name ,$options : '$i'}}) 
    
    .then(result => {
        if(result.length === 0){
            return res.send('Product is not available at the moment')
        } else {
            return res.send(result)
        }
    })
}

// Sorting Products in Ascending Order
module.exports.sortAscending = (req, res) => {
    Product.find({})
        .sort({name : 1})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// Sorting Products in Descending Order

module.exports.sortDescending = (req, res) => {
    Product.find({})
        .sort({name : -1})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


module.exports.category = (req, res) => {
    console.log(req.body)
    Product.find({category : {$regex : req.body.category ,$options : '$i'}})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

// module.exports.searchByCategory = (req, res) => {
//     console.log(req.body)
//     Product.find({category : {$regex : req.body.category ,$options : '$i'}}) 
    
//     .then(result => {
//         if(result.length === 0){
//             return res.send('Product is not available at the moment')
//         } else {
//             return res.send(result)
//         }
//     })
// }

module.exports.deactivateProduct = (req, res) => {
    console.log(req.body)
    let updates = {
        isActive: false
    }
    Product.findByIdAndUpdate(req.params.id,updates, {new: true} )
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


//! Purchase History
module.exports.purchaseHistory = (req, res) => {
    console.log(req.body)
    Product.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))
}


// !View Specific Product
module.exports.viewProduct = (req, res) => {
    console.log(req.body)
    Product.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
}

